package test.delibere.poc.workitemhandler;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;


public class MyCustomTestTaskWorkItemHandler implements WorkItemHandler{

	@Override
	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
		
		long id=workItem.getProcessInstanceId();
		String param1=(String)workItem.getParameter("param1");
		String param2=(String)workItem.getParameter("param2");
		Object param3=(Object)workItem.getParameter("param3");
		
		Map<String,Object> results=new HashMap<String,Object>(); 
		results.put("output1", "output1_value:"+param1);
		results.put("output2", "output2_value:"+param2);
		results.put("output3", "output3_value:"+param3);
		
		Map<String,Object> results2=new HashMap<String,Object>(); 
		results2.put("output1", results);
		
		System.out.println("----MyCustomTestTaskWorkItemHandler - executeWorkItem-----ProcessInstanceId= " + id);
		System.out.println("----param1="+param1+" param2="+param2+" param3="+param3);
		
		workItemManager.completeWorkItem(workItem.getId(), results2);
	
	}

}
