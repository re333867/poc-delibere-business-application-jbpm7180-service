package test.delibere.poc.workitemhandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;

public class MyNewCustomTaskWorkItemHandler implements WorkItemHandler{

	
	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
		long id=workItem.getProcessInstanceId();
				
		String input1=(String)workItem.getParameter("input1");
		Map<String,Object> input2=(Map<String, Object>)workItem.getParameter("input2");
		
		System.out.println("********MyNewCustomTaskWorkItemHandler - executeWorkItem-----ProcessInstanceId= " + id);
		Set<String> pars=workItem.getParameters().keySet();
		pars.forEach(k->{System.out.println("****MyNewCustomTaskWorkItemHandler - "+k+" - "+workItem.getParameter(k));});
		
		System.out.println("********MyNewCustomTaskWorkItemHandler input1="+input1+" -input2="+input2);		
		input2.put("nuova_key_inserita","-ROSARIO ESPOSITO-");
		
		
		Map<String,Object> results=new HashMap<String,Object>(); 
		results.put("output1", input2);
		
		workItemManager.completeWorkItem(workItem.getId(), results);
	}


	@Override
	public void abortWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
			
	}

	
}
