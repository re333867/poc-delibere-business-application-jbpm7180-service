package test.delibere.poc.listener;

import org.kie.api.task.TaskEvent;
import org.kie.api.task.TaskLifeCycleEventListener;

public class MyTaskEventListener implements TaskLifeCycleEventListener{

	
	@Override
	public void beforeTaskStartedEvent(TaskEvent ev) {
		Long taskId=ev.getTask().getId();
		String taskName=ev.getTask().getName();	
		System.out.println("MyTaskEventListener - beforeTaskStartedEvent "+taskId + " -- "+taskName);
		
	}
	@Override
	public void afterTaskActivatedEvent(TaskEvent ev) {
		Long taskId=ev.getTask().getId();
		String taskName=ev.getTask().getName();	
		System.out.println("MyTaskEventListener - afterTaskActivatedEvent "+taskId + " -- "+taskName);
				
	}

	@Override
	public void afterTaskAddedEvent(TaskEvent ev) {
		Long taskId=ev.getTask().getId();
		String taskName=ev.getTask().getName();	
		System.out.println("MyTaskEventListener - afterTaskAddedEvent "+taskId + " -- "+taskName);				
	}

	@Override
	public void afterTaskClaimedEvent(TaskEvent ev) {
		Long taskId=ev.getTask().getId();
		String taskName=ev.getTask().getName();	
		System.out.println("MyTaskEventListener - afterTaskClaimedEvent "+taskId + " -- "+taskName);				
	
		
	}

	@Override
	public void afterTaskCompletedEvent(TaskEvent ev) {
		Long taskId=ev.getTask().getId();
		String taskName=ev.getTask().getName();	
		System.out.println("MyTaskEventListener - afterTaskCompletedEvent "+taskId + " -- "+taskName);				
	
		
	}

	@Override
	public void afterTaskDelegatedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTaskExitedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTaskFailedEvent(TaskEvent ev) {
		Long taskId=ev.getTask().getId();
		String taskName=ev.getTask().getName();	
		System.out.println("MyTaskEventListener - afterTaskFailedEvent "+taskId + " -- "+taskName);				
	
	}

	@Override
	public void afterTaskForwardedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTaskNominatedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTaskReleasedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTaskResumedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTaskSkippedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTaskStartedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTaskStoppedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTaskSuspendedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskActivatedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskAddedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskClaimedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskCompletedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskDelegatedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskExitedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskFailedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskForwardedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskNominatedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskReleasedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskResumedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskSkippedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	

	@Override
	public void beforeTaskStoppedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTaskSuspendedEvent(TaskEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
