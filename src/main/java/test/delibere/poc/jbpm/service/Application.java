package test.delibere.poc.jbpm.service;

import java.util.ArrayList;
import java.util.List;

import org.jbpm.springboot.security.SpringSecurityUserGroupCallback;
import org.kie.api.task.UserGroupCallback;
import org.kie.internal.identity.IdentityProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	@Bean
    @ConditionalOnMissingBean(name = "userGroupCallback")
    public UserGroupCallback userGroupCallback(IdentityProvider identityProvider) {
       System.out.println("<<<<<<<<<<<<<<<<<<<<<<userGroupCallback>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	   //return new SpringSecurityUserGroupCallback(identityProvider);
		return new UserGroupCallback() {
			
			@Override
			public List<String> getGroupsForUser(String userId) {
				return new ArrayList<>();
			}
			
			@Override
			public boolean existsUser(String userId) {
				return true;
			}
			
			@Override
			public boolean existsGroup(String groupId) {
				return true;
				
			}
		};
	}
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}