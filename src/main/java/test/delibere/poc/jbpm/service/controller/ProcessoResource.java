package test.delibere.poc.jbpm.service.controller;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProcessoResource {
	
	private String idProcesso;
	private String processName;
	private String note;
	
	public String getIdProcesso() {
		return idProcesso;
	}
	public void setIdProcesso(String idProcesso) {
		this.idProcesso = idProcesso;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
}
