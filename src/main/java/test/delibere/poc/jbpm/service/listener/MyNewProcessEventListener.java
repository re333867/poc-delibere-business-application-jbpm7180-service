package test.delibere.poc.jbpm.service.listener;

import java.util.Collection;
import java.util.List;

import org.jbpm.process.audit.AuditLogService;
import org.jbpm.process.audit.VariableInstanceLog;
import org.jbpm.services.api.ProcessService;
import org.jbpm.services.api.RuntimeDataService;
import org.jbpm.services.api.model.VariableDesc;
import org.kie.api.event.process.ProcessCompletedEvent;
import org.kie.api.event.process.ProcessEventListener;
import org.kie.api.event.process.ProcessNodeLeftEvent;
import org.kie.api.event.process.ProcessNodeTriggeredEvent;
import org.kie.api.event.process.ProcessStartedEvent;
import org.kie.api.event.process.ProcessVariableChangedEvent;
import org.kie.api.runtime.manager.audit.AuditService;
import org.kie.api.runtime.query.QueryContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

 
/**
 * 
 * Nota: possibile estendere anche org.kie.api.event.process.DefaultProcessEventListener
 * per evitare implementazione di tutti i metodi 
 * 
 * 
 * @author R.Esposito
 *
 */
@Component
public class MyNewProcessEventListener implements ProcessEventListener{

	@Autowired
	private ProcessService processService;

	@Autowired
	private RuntimeDataService runtimeDataService;


	@Override
	public void beforeProcessStarted(ProcessStartedEvent ev) {		
		System.out.println("MyNewProcessEventListener - start beforeProcessStarted "+ev.getProcessInstance().getId()+" "+ev.getProcessInstance().getProcessName());	
	}
	
	@Override
	public void afterProcessStarted(ProcessStartedEvent ev) {
		System.out.println("MyNewProcessEventListener - start afterProcessStarted "+ev.getProcessInstance().getId()+" "+ev.getProcessInstance().getProcessName());								
	}
	
	@Override
	public void afterProcessCompleted(ProcessCompletedEvent ev) {
		System.out.println("MyNewProcessEventListener - start afterProcessCompleted "+ev.getProcessInstance().getId()+" "+ev.getProcessInstance().getProcessName());	
		System.out.println("????????????????????????????>>>>>"+this.processService);
		
		/*Map<String,Object>  vars=this.processService.getProcessInstanceVariables(ev.getProcessInstance().getId());
		if (vars!=null) {
			vars.keySet().forEach(k->{System.out.println("var=" + k + " - value= " + vars.get(k));});
		}
		Collection<org.jbpm.services.api.model.NodeInstanceDesc> x=runtimeDataService.getProcessInstanceFullHistory(ev.getProcessInstance().getId(), new QueryContext());
		x.forEach(k->{System.out.println("var=" + k );});
		*/
		
		Collection<org.jbpm.services.api.model.NodeInstanceDesc> x=runtimeDataService.getProcessInstanceFullHistory(ev.getProcessInstance().getId(), new QueryContext());
		x.forEach(k->{System.out.println("var=" + k );});
	
		Collection<VariableDesc> x1=runtimeDataService.getVariablesCurrentState(ev.getProcessInstance().getId());
		x1.forEach(k->{System.out.println("var=" + k );});
	
	}
	
	@Override
	public void afterNodeLeft(ProcessNodeLeftEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterNodeTriggered(ProcessNodeTriggeredEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	

	

	@Override
	public void afterVariableChanged(ProcessVariableChangedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeNodeLeft(ProcessNodeLeftEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeNodeTriggered(ProcessNodeTriggeredEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeProcessCompleted(ProcessCompletedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	

	@Override
	public void beforeVariableChanged(ProcessVariableChangedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
