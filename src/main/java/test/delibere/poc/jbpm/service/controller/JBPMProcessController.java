package test.delibere.poc.jbpm.service.controller;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.collections4.map.HashedMap;
import org.jbpm.services.api.DefinitionService;
import org.jbpm.services.api.DeploymentService;
import org.jbpm.services.api.ProcessService;
import org.jbpm.services.api.RuntimeDataService;
import org.jbpm.services.api.UserTaskService;
import org.jbpm.services.api.admin.ProcessInstanceMigrationService;
import org.jbpm.services.api.admin.UserTaskAdminService;
import org.jbpm.services.api.query.QueryService;
import org.kie.api.task.model.Content;
import org.kie.api.task.model.Task;
import org.kie.internal.KieInternalServices;
import org.kie.internal.process.CorrelationKey;
import org.kie.internal.process.CorrelationKeyFactory;
import org.kie.server.services.api.KieServer;
import org.kie.server.springboot.jbpm.ContainerAliasResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
/*ProcessService
DeploymentService
DefinitionService
RuntimeDataService
UserTaskService
ProcessInstanceAdminService
UserTaskAdminService
CaseService
CaseRuntimeDataService
*/
@RestController
public class JBPMProcessController {

	@Autowired
	private ProcessService processService;
	
	@Autowired
	private UserTaskAdminService userTaskAdminService;

	@Autowired
	private RuntimeDataService runtimeDataService;

	@Autowired
	private UserTaskService userTaskService;;

	@Autowired
	private DeploymentService deploymentService;

	
	@Autowired
	private DefinitionService definitionService;

	@Autowired
	private QueryService queryService;

	@Autowired
	private ProcessInstanceMigrationService processInstanceMigrationService;

	@Autowired
	private KieServer kieServer;

	@Autowired
	private ContainerAliasResolver aliasResolver;

	@GetMapping(value = "/avviaWF", produces = "application/json")
	@ApiOperation(value = "Avvia WF delibera")
	public ProcessoResource avviaWF() {

		StringWriter st = new StringWriter();
		deploymentService.getDeployedUnits().forEach(du -> {
			st.append(du.getDeploymentUnit().getIdentifier()).append(du.getDeploymentUnit().getStrategy().name());
		});
			
		Map<String,Object> processParameters=new HashedMap<String,Object>();
		processParameters.put("processVar1", "processVar1Value");
		processParameters.put("processVar2", "processVar2Value");
		
		
		CorrelationKeyFactory factory=KieInternalServices.Factory.get().newCorrelationKeyFactory();
		String correlationKeyST=UUID.randomUUID().toString();
		System.out.println("correlationKey="+correlationKeyST);
		CorrelationKey correlationKey= factory.newCorrelationKey(correlationKeyST);
		
		
		Long id = processService.startProcess("poc-delibere-business-application-jbpm7180-kjar-1_0-SNAPSHOT",
				"TestProcesso3",correlationKey,processParameters);
		 
		ProcessoResource t = new ProcessoResource();
		t.setIdProcesso(id.toString());
		t.setProcessName("XXXXXXX");
		return t;
	}
	
	/////////////////////////////////////////////////////
	
	@GetMapping(value = "/avviaWF2", produces = "application/json")
	@ApiOperation(value = "Avvia WF delibera")
	public ProcessoResource avviaW2F() {

		StringWriter st = new StringWriter();
		deploymentService.getDeployedUnits().forEach(du -> {
			st.append(du.getDeploymentUnit().getIdentifier()).append(du.getDeploymentUnit().getStrategy().name());
		});
			
		Map<String,Object> processParameters=new HashedMap<String,Object>();
		processParameters.put("processVar1", "processVar1Value");
		processParameters.put("processVar2", "processVar2Value");
		processParameters.put("processVar3", "processVar3Value");
		processParameters.put("processVar4", new HashMap<String,Object>());
		
		
		CorrelationKeyFactory factory=KieInternalServices.Factory.get().newCorrelationKeyFactory();
		String correlationKeyST=UUID.randomUUID().toString();
		System.out.println("correlationKey="+correlationKeyST);
		CorrelationKey correlationKey= factory.newCorrelationKey(correlationKeyST);
		
		
		Long id = processService.startProcess("poc-delibere-business-application-jbpm7180-kjar-1_0-SNAPSHOT",
				"TestProcesso2",correlationKey,processParameters);
		 
		ProcessoResource t = new ProcessoResource();
		t.setIdProcesso(id.toString());
		t.setProcessName("----TestProcesso2---key="+correlationKey.toString());
		return t;
	}

	
	@GetMapping(value = "/getProcess/{key}", produces = "application/json")
	@ApiOperation(value = "Avvia WF delibera")
	public ProcessoResource getProcessInstanceByKey(@PathVariable("key") String key) {
		
		
		CorrelationKeyFactory factory=KieInternalServices.Factory.get().newCorrelationKeyFactory();
		CorrelationKey correlationKey=factory.newCorrelationKey(key);
		org.jbpm.services.api.model.ProcessInstanceDesc pi= this.runtimeDataService.getProcessInstanceByCorrelationKey(correlationKey);
		System.out.println(">>>>>"+pi.getId()+" "+pi.getProcessName());
		
		ProcessoResource t = new ProcessoResource();
		t.setIdProcesso(String.valueOf(pi.getId()));
		t.setNote("NOTE:"+pi.toString()+"    ---TASK:  ");
		
		List<Long> taskIds=runtimeDataService.getTasksByProcessInstanceId(pi.getId());
		taskIds.forEach(x->{
			Task task=userTaskService.getTask(x);
			
			StringWriter st=new StringWriter();
			st.append(task.getId().toString()).append(" ");
			st.append(task.getDescription()).append(" ");
			st.append(task.getName()).append(" ");
			st.append(task.getTaskData().getActualOwner()+" ");
			st.append(task.getTaskData().getCreatedBy()+" ");
			st.append(task.getPeopleAssignments().getPotentialOwners()+" ");
			st.append(task.getPeopleAssignments().getTaskInitiator()+" ");
			
			
			st.append("stato="+task.getTaskData().getStatus().toString()).append(" ");			
			st.append(task.getTaskType()).append(" ");
			st.append(">>>>>>>>>"+userTaskService.getTaskInputContentByTaskId(task.getId()));
			st.append(">>>>>>>>>"+userTaskService.getTaskOutputContentByTaskId(task.getId()));
			t.setNote(t.getNote()+" ---- "+st);		
		});
		return t;		
	}
	
	@GetMapping(value = "/completeTask/{taskId}", produces = "application/json")
	@ApiOperation(value = "Avvia WF delibera")
	public ProcessoResource completeTask(@PathVariable("taskId") String taskId) {
		
		Task task=userTaskService.getTask(Long.valueOf(taskId));
		System.out.println("task trovato "+task.getPeopleAssignments());
		System.out.println("task trovato "+task.getTaskData().getActualOwner());
		
		
		
		//this.userTaskService.claim(task.getId(), "rosario");
		this.userTaskService.start(task.getId(), "rosario");
		
		Map<String,Object> result=new HashMap<String,Object>();
		result.put("output1", "OK - OUT TASK - ");
		this.userTaskService.complete(task.getId(), "rosario", result);
		
		ProcessoResource t = new ProcessoResource();
		t.setIdProcesso("ID_TASK="+String.valueOf(task.getId()));
		return t;
	}
	
	
}
